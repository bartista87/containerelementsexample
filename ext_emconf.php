<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "containerelementsexample"
 *
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Example EXT:Container Elements',
    'description' => 'Example EXT:Container elements',
    'category' => 'backend',
    'author' => 'Stefan Bublies',
    'author_email' => 'project@sbublies.de',
    'state' => 'beta',
    'uploadfolder' => 1,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '11.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
